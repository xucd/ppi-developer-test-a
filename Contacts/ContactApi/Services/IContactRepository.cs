﻿using ContactApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApi.Services
{
    public interface IContactRepository
    {
        Task<List<Location>> GetLocations();
    }
}
