﻿using ContactApi.Entities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContactApi.Services
{
    public class ContactRepository : IContactRepository
    {
        //The URL of the contacts WEB API Service
        readonly string _sourceUrl;
        HttpClient _client;

        public ContactRepository(IConfiguration configuration)
        {
            _sourceUrl = "https://ppipubsiteservices.azurewebsites.net/api/contacts/en";
            _client = new HttpClient();
        }

        /// <summary>
        /// Get all office locations from the web api
        /// </summary>
        public async Task<List<Location>> GetLocations()
        {
            var response = await _client.GetAsync(_sourceUrl);
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var locations = JsonConvert.DeserializeObject<List<Location>>(json);
                return locations;
            }

            return new List<Location>();
        }
    }
}
