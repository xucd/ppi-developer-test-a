﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace ContactApi.Controllers
{
    [Route("api")]
    public class HomeController : ControllerBase
    {
        private readonly IContactRepository _contactRepository;

        public HomeController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        // GET api?query=...
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string query)
        {
            // Retrieve all office locations from the web api
            var locations = await _contactRepository.GetLocations();
            if (locations == null) return NotFound();

            // Return if no filter is specified
            if (string.IsNullOrWhiteSpace(query))
                return Ok(locations);

            // Check if the query matches an office
            var office = locations.FirstOrDefault(l => query.Equals(l.City, StringComparison.OrdinalIgnoreCase));
            if (office != null)
                return Ok(office);

            // Check if the query matches a contact
            var contact = (from l in locations
                           from cat in l.Contacts_by_category
                           from con in cat.Contacts
                           where query.Equals(con.Name, StringComparison.OrdinalIgnoreCase)
                           select con).FirstOrDefault();
            if (contact != null)
                return Ok(contact);

            return NotFound();
        }
    }
}
