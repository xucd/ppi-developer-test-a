﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApi.Entities
{
    public class Location
    {
        public int Id { get; set; }
        public string Office { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Province { get; set; }
        public string Postal { get; set; }
        public Phone Phone { get; set; }
        public GpsCoordinates Coordinates { get; set; }
        public List<Tag> Tags { get; set; }
        public List<ContactByCategory> Contacts_by_category { get; set; }
    }
}
