﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApi.Entities
{
    public class ContactByCategory
    {
        public string Category { get; set; }
        public List<Contact> Contacts { get; set; }
    }
}
