﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApi.Entities
{
    public class GpsCoordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
