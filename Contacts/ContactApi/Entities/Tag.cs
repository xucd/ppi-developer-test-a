﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApi.Entities
{
    public class Tag
    {
        public string Slug { get; set; }
        public string Company { get; set; }
    }
}
