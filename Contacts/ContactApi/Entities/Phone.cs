﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApi.Entities
{
    public class Phone
    {
        public string Direct { get; set; }
        public string Fax { get; set; }
        public string Tollfree { get; set; }
    }
}
